<?php
	/**
	 * Class Surat Model
	 */
	class Surat_model extends CI_Model
	{
		function generateResi(){
			$tgl = date('dmY');
			$jam = date('His');
			$karakter = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
			$shuffle  = substr(str_shuffle($karakter), 0, 6);
			$resi = $jam.$shuffle.$tgl;
			return $resi;
		}

		function generatePswd(){
			$tgl = date('dmY');
			$jam = date('His');
			$karakter = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
			$shuffle  = substr(str_shuffle($karakter), 0, 6);
			$resi = $tgl.$shuffle.$jam;
			return $resi;
		}

		function getArsipbyResi($resi){
			$this->db->where('resi', $resi);
			$this->db->get('t_arsip')->row();
		}

		function NoSurat($nosurat){
			$awal = date('Y-m-01');
			$akhir = date('Y-m-d');
			$query = $this->db->query("SELECT COUNT(no_surat)+1 as count FROM t_arsip WHERE jenis_surat = '$nosurat' AND tgl_pengantar BETWEEN '$awal' AND '$akhir'")->row();
			$no = $query->count;
			return $no;
		}

		function Simpan($data){
			unset($data['submit_form']);
			$this->db->insert('t_arsip', $data);
		}

		function getSurat(){
			$this->datatables->select('*');
	        $this->datatables->from('t_arsip');
	        //add this line for join
	        $this->datatables->join('t_penduduk', 't_penduduk.id = t_arsip.id_penduduk');
	        $this->datatables->add_column('action',anchor(site_url('user/delete/$1'),'<i class="fa fa-trash-o" aria-hidden="true"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id_users');
	        return $this->datatables->generate();
		}

		function ceksurat($jenis,$id){
			$this->db->where('jenis_surat', $jenis);
			$this->db->where('no_surat', $id);

			return $this->db->count_all_results('t_arsip');
		}
	}
?>