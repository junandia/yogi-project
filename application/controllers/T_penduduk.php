<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class T_penduduk extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('T_penduduk_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->uri->segment(3));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . '.php/c_url/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'index.php/t_penduduk/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'index.php/t_penduduk/index/';
            $config['first_url'] = base_url() . 'index.php/t_penduduk/index/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = FALSE;
        $config['total_rows'] = $this->T_penduduk_model->total_rows($q);
        $t_penduduk = $this->T_penduduk_model->get_limit_data($config['per_page'], $start, $q);
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            't_penduduk_data' => $t_penduduk,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template','t_penduduk/t_penduduk_list', $data);
    }

    public function read($id) 
    {
        $row = $this->T_penduduk_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'no_kk' => $row->no_kk,
		'no_nik' => $row->no_nik,
		'nama' => $row->nama,
		'tempat_lahir' => $row->tempat_lahir,
		'tgl_lahir' => $row->tgl_lahir,
		'jenis_k' => $row->jenis_k,
		'agama' => $row->agama,
		'pendidikan' => $row->pendidikan,
		'pekerjaan' => $row->pekerjaan,
		's_kawin' => $row->s_kawin,
		's_keluarga' => $row->s_keluarga,
		'kewarganegaraan' => $row->kewarganegaraan,
		'alamat' => $row->alamat,
		'rt' => $row->rt,
		'rw' => $row->rw,
		'kelurahan' => $row->kelurahan,
		'kecamatan' => $row->kecamatan,
		'kab_kota' => $row->kab_kota,
		'kode_pos' => $row->kode_pos,
		'provinsi' => $row->provinsi,
		'type_penduduk' => $row->type_penduduk,
		'last_update' => $row->last_update,
	    );
            $this->template->load('template','t_penduduk/t_penduduk_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('t_penduduk'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('t_penduduk/create_action'),
	    'id' => set_value('id'),
	    'no_kk' => set_value('no_kk'),
	    'no_nik' => set_value('no_nik'),
	    'nama' => set_value('nama'),
	    'tempat_lahir' => set_value('tempat_lahir'),
	    'tgl_lahir' => set_value('tgl_lahir'),
	    'jenis_k' => set_value('jenis_k'),
	    'agama' => set_value('agama'),
	    'pendidikan' => set_value('pendidikan'),
	    'pekerjaan' => set_value('pekerjaan'),
	    's_kawin' => set_value('s_kawin'),
	    's_keluarga' => set_value('s_keluarga'),
	    'kewarganegaraan' => set_value('kewarganegaraan'),
	    'alamat' => set_value('alamat'),
	    'rt' => set_value('rt'),
	    'rw' => set_value('rw'),
	    'kelurahan' => set_value('kelurahan'),
	    'kecamatan' => set_value('kecamatan'),
	    'kab_kota' => set_value('kab_kota'),
	    'kode_pos' => set_value('kode_pos'),
	    'provinsi' => set_value('provinsi'),
	    'type_penduduk' => set_value('type_penduduk'),
	    'last_update' => set_value('last_update'),
	);
        $this->template->load('template','t_penduduk/t_penduduk_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'no_kk' => $this->input->post('no_kk',TRUE),
		'no_nik' => $this->input->post('no_nik',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'tempat_lahir' => $this->input->post('tempat_lahir',TRUE),
		'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
		'jenis_k' => $this->input->post('jenis_k',TRUE),
		'agama' => $this->input->post('agama',TRUE),
		'pendidikan' => $this->input->post('pendidikan',TRUE),
		'pekerjaan' => $this->input->post('pekerjaan',TRUE),
		's_kawin' => $this->input->post('s_kawin',TRUE),
		's_keluarga' => $this->input->post('s_keluarga',TRUE),
		'kewarganegaraan' => $this->input->post('kewarganegaraan',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'rt' => $this->input->post('rt',TRUE),
		'rw' => $this->input->post('rw',TRUE),
		'kelurahan' => $this->input->post('kelurahan',TRUE),
		'kecamatan' => $this->input->post('kecamatan',TRUE),
		'kab_kota' => $this->input->post('kab_kota',TRUE),
		'kode_pos' => $this->input->post('kode_pos',TRUE),
		'provinsi' => $this->input->post('provinsi',TRUE),
		'type_penduduk' => $this->input->post('type_penduduk',TRUE),
		'last_update' => $this->input->post('last_update',TRUE),
	    );

            $this->T_penduduk_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('t_penduduk'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->T_penduduk_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('t_penduduk/update_action'),
		'id' => set_value('id', $row->id),
		'no_kk' => set_value('no_kk', $row->no_kk),
		'no_nik' => set_value('no_nik', $row->no_nik),
		'nama' => set_value('nama', $row->nama),
		'tempat_lahir' => set_value('tempat_lahir', $row->tempat_lahir),
		'tgl_lahir' => set_value('tgl_lahir', $row->tgl_lahir),
		'jenis_k' => set_value('jenis_k', $row->jenis_k),
		'agama' => set_value('agama', $row->agama),
		'pendidikan' => set_value('pendidikan', $row->pendidikan),
		'pekerjaan' => set_value('pekerjaan', $row->pekerjaan),
		's_kawin' => set_value('s_kawin', $row->s_kawin),
		's_keluarga' => set_value('s_keluarga', $row->s_keluarga),
		'kewarganegaraan' => set_value('kewarganegaraan', $row->kewarganegaraan),
		'alamat' => set_value('alamat', $row->alamat),
		'rt' => set_value('rt', $row->rt),
		'rw' => set_value('rw', $row->rw),
		'kelurahan' => set_value('kelurahan', $row->kelurahan),
		'kecamatan' => set_value('kecamatan', $row->kecamatan),
		'kab_kota' => set_value('kab_kota', $row->kab_kota),
		'kode_pos' => set_value('kode_pos', $row->kode_pos),
		'provinsi' => set_value('provinsi', $row->provinsi),
		'type_penduduk' => set_value('type_penduduk', $row->type_penduduk),
		'last_update' => set_value('last_update', $row->last_update),
	    );
            $this->template->load('template','t_penduduk/t_penduduk_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('t_penduduk'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'no_kk' => $this->input->post('no_kk',TRUE),
		'no_nik' => $this->input->post('no_nik',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'tempat_lahir' => $this->input->post('tempat_lahir',TRUE),
		'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
		'jenis_k' => $this->input->post('jenis_k',TRUE),
		'agama' => $this->input->post('agama',TRUE),
		'pendidikan' => $this->input->post('pendidikan',TRUE),
		'pekerjaan' => $this->input->post('pekerjaan',TRUE),
		's_kawin' => $this->input->post('s_kawin',TRUE),
		's_keluarga' => $this->input->post('s_keluarga',TRUE),
		'kewarganegaraan' => $this->input->post('kewarganegaraan',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'rt' => $this->input->post('rt',TRUE),
		'rw' => $this->input->post('rw',TRUE),
		'kelurahan' => $this->input->post('kelurahan',TRUE),
		'kecamatan' => $this->input->post('kecamatan',TRUE),
		'kab_kota' => $this->input->post('kab_kota',TRUE),
		'kode_pos' => $this->input->post('kode_pos',TRUE),
		'provinsi' => $this->input->post('provinsi',TRUE),
		'type_penduduk' => $this->input->post('type_penduduk',TRUE),
		'last_update' => $this->input->post('last_update',TRUE),
	    );

            $this->T_penduduk_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('t_penduduk'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->T_penduduk_model->get_by_id($id);

        if ($row) {
            $this->T_penduduk_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('t_penduduk'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('t_penduduk'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('no_kk', 'no kk', 'trim|required');
	$this->form_validation->set_rules('no_nik', 'no nik', 'trim|required');
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required');
	$this->form_validation->set_rules('tgl_lahir', 'tgl lahir', 'trim|required');
	$this->form_validation->set_rules('jenis_k', 'jenis k', 'trim|required');
	$this->form_validation->set_rules('agama', 'agama', 'trim|required');
	$this->form_validation->set_rules('pendidikan', 'pendidikan', 'trim|required');
	$this->form_validation->set_rules('pekerjaan', 'pekerjaan', 'trim|required');
	$this->form_validation->set_rules('s_kawin', 's kawin', 'trim|required');
	$this->form_validation->set_rules('s_keluarga', 's keluarga', 'trim|required');
	$this->form_validation->set_rules('kewarganegaraan', 'kewarganegaraan', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('rt', 'rt', 'trim|required');
	$this->form_validation->set_rules('rw', 'rw', 'trim|required');
	$this->form_validation->set_rules('kelurahan', 'kelurahan', 'trim|required');
	$this->form_validation->set_rules('kecamatan', 'kecamatan', 'trim|required');
	$this->form_validation->set_rules('kab_kota', 'kab kota', 'trim|required');
	$this->form_validation->set_rules('kode_pos', 'kode pos', 'trim|required');
	$this->form_validation->set_rules('provinsi', 'provinsi', 'trim|required');
	$this->form_validation->set_rules('type_penduduk', 'type penduduk', 'trim|required');
	$this->form_validation->set_rules('last_update', 'last update', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "t_penduduk.xls";
        $judul = "t_penduduk";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "No Kk");
	xlsWriteLabel($tablehead, $kolomhead++, "No Nik");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama");
	xlsWriteLabel($tablehead, $kolomhead++, "Tempat Lahir");
	xlsWriteLabel($tablehead, $kolomhead++, "Tgl Lahir");
	xlsWriteLabel($tablehead, $kolomhead++, "Jenis K");
	xlsWriteLabel($tablehead, $kolomhead++, "Agama");
	xlsWriteLabel($tablehead, $kolomhead++, "Pendidikan");
	xlsWriteLabel($tablehead, $kolomhead++, "Pekerjaan");
	xlsWriteLabel($tablehead, $kolomhead++, "S Kawin");
	xlsWriteLabel($tablehead, $kolomhead++, "S Keluarga");
	xlsWriteLabel($tablehead, $kolomhead++, "Kewarganegaraan");
	xlsWriteLabel($tablehead, $kolomhead++, "Alamat");
	xlsWriteLabel($tablehead, $kolomhead++, "Rt");
	xlsWriteLabel($tablehead, $kolomhead++, "Rw");
	xlsWriteLabel($tablehead, $kolomhead++, "Kelurahan");
	xlsWriteLabel($tablehead, $kolomhead++, "Kecamatan");
	xlsWriteLabel($tablehead, $kolomhead++, "Kab Kota");
	xlsWriteLabel($tablehead, $kolomhead++, "Kode Pos");
	xlsWriteLabel($tablehead, $kolomhead++, "Provinsi");
	xlsWriteLabel($tablehead, $kolomhead++, "Type Penduduk");
	xlsWriteLabel($tablehead, $kolomhead++, "Last Update");

	foreach ($this->T_penduduk_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->no_kk);
	    xlsWriteLabel($tablebody, $kolombody++, $data->no_nik);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tempat_lahir);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tgl_lahir);
	    xlsWriteLabel($tablebody, $kolombody++, $data->jenis_k);
	    xlsWriteLabel($tablebody, $kolombody++, $data->agama);
	    xlsWriteLabel($tablebody, $kolombody++, $data->pendidikan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->pekerjaan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->s_kawin);
	    xlsWriteLabel($tablebody, $kolombody++, $data->s_keluarga);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kewarganegaraan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->alamat);
	    xlsWriteLabel($tablebody, $kolombody++, $data->rt);
	    xlsWriteLabel($tablebody, $kolombody++, $data->rw);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kelurahan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kecamatan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kab_kota);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kode_pos);
	    xlsWriteLabel($tablebody, $kolombody++, $data->provinsi);
	    xlsWriteLabel($tablebody, $kolombody++, $data->type_penduduk);
	    xlsWriteLabel($tablebody, $kolombody++, $data->last_update);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
}