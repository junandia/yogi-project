<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Arsip extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Arsip_model');
        $this->load->model('Surat_model');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template','arsip/t_arsip_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Arsip_model->json();
    }

    public function print($id) 
    {
        $row = $this->Arsip_model->get_by_id($id);
        if ($row) {
            $text = "KELURAHAN KARANGTENGAH\n \n NAMA : ".$row->nama."\n"."JENIS SURAT : ".strtoupper($row->jenis_surat)."\n\nBetul Dikeluarkan oleh Kelurahan Karang Tengah dengan Nomor Resi ".$row->resi;
            $resi = chiper_encrypt('yulistiana', $text);
            $resi2 = chiper_encrypt('yogi', $resi); 
            $resi3 = chiper_encrypt ('syra', $resi2);
            $resi =  base64_encode($resi3);
            //var_dump($resi);
                    $params['data'] = $resi;
                    $params['level'] = 'H';
                    $params['size'] = 2;
                    $params['savename'] = FCPATH.'/assets/images/qrcode.png';
                    $this->ciqrcode->generate($params);
            $data = array(
        		'id' => $row->id,
        		'jenis_surat' => $row->jenis_surat,
        		'no_surat' => $row->no_surat,
        		'id_penduduk' => $row->id_penduduk,
        		'no_suratpengantar' => $row->no_suratpengantar,
        		'tgl_pengantar' => $row->tgl_pengantar,
        		'tujuan_pembuatan' => $row->tujuan_pembuatan,
        		'resi' => $row->resi,
        		'created_at' => $row->created_at,
        		'tanggal_kematian' => $row->tanggal_kematian,
        		'jam_kematian' => $row->jam_kematian,
        		'penyebab_kematian' => $row->penyebab_kematian,
                'no_kk' => $row->no_kk,
                'no_nik' => $row->no_nik,
                'nama' => $row->nama,
                'tempat_lahir' => $row->tempat_lahir,
                'tgl_lahir' => $row->tgl_lahir,
                'jenis_k' => $row->jenis_k,
                'agama' => $row->agama,
                'pendidikan' => $row->pendidikan,
                'pekerjaan' => $row->pekerjaan,
                's_kawin' => $row->s_kawin,
                's_keluarga' => $row->s_keluarga,
                'kewarganegaraan' => $row->kewarganegaraan,
                'alamat' => $row->alamat,
                'rt' => $row->rt,
                'rw' => $row->rw,
                'kelurahan' => $row->kelurahan,
                'kecamatan' => $row->kecamatan,
                'kab_kota' => $row->kab_kota,
                'kode_pos' => $row->kode_pos,
                'provinsi' => $row->provinsi,
                'type_penduduk' => $row->type_penduduk,
                'last_update' => $row->last_update,
                'qrcode' => base_url().'assets/images/qrcode.png'
	    );
        $file = 'surat/preview_'.$row->jenis_surat;
            $this->template->load('previewSurat',$file, $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('arsip'));
        }
    }


    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "t_arsip.xls";
        $judul = "t_arsip";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Jenis Surat");
	xlsWriteLabel($tablehead, $kolomhead++, "No Surat");
	xlsWriteLabel($tablehead, $kolomhead++, "No KK");
    xlsWriteLabel($tablehead, $kolomhead++, "NIK");
    xlsWriteLabel($tablehead, $kolomhead++, "Nama");
    xlsWriteLabel($tablehead, $kolomhead++, "Tempat Lahir");
    xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Lahir");
    xlsWriteLabel($tablehead, $kolomhead++, "Alamat");
	xlsWriteLabel($tablehead, $kolomhead++, "No Suratpengantar");
	xlsWriteLabel($tablehead, $kolomhead++, "Tgl Pengantar");
	xlsWriteLabel($tablehead, $kolomhead++, "Tujuan Pembuatan");
	xlsWriteLabel($tablehead, $kolomhead++, "Resi");
	xlsWriteLabel($tablehead, $kolomhead++, "Created At");
	xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Kematian");
	xlsWriteLabel($tablehead, $kolomhead++, "Jam Kematian");
	xlsWriteLabel($tablehead, $kolomhead++, "Penyebab Kematian");

	foreach ($this->Arsip_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->jenis_surat);
	    xlsWriteLabel($tablebody, $kolombody++, $data->no_surat);
	    xlsWriteLabel($tablebody, $kolombody++, $data->no_kk);
        xlsWriteLabel($tablebody, $kolombody++, $data->no_nik);
        xlsWriteLabel($tablebody, $kolombody++, $data->nama);
        xlsWriteLabel($tablebody, $kolombody++, $data->tempat_lahir);
        xlsWriteLabel($tablebody, $kolombody++, $data->tgl_lahir);
        xlsWriteLabel($tablebody, $kolombody++, $data->alamat);
	    xlsWriteLabel($tablebody, $kolombody++, $data->no_suratpengantar);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tgl_pengantar);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tujuan_pembuatan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->resi);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tanggal_kematian);
	    xlsWriteLabel($tablebody, $kolombody++, $data->jam_kematian);
	    xlsWriteLabel($tablebody, $kolombody++, $data->penyebab_kematian);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Arsip.php */
/* Location: ./application/controllers/Arsip.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-07-24 16:09:18 */
/* http://harviacode.com */