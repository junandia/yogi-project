<?php
	/**
	 * Surat Class
	 */
	class Surat extends CI_Controller
	{

		public function __construct()
		{
			parent::__construct();
			is_login();
			$this->load->model('Surat_model');
			$this->load->model('T_penduduk_model');
		}

		public function form_domisili($preview = false){
			if ($preview == false) {
				$data = [
					'penduduk' => $this->T_penduduk_model->get_all(),
					'resi' => $this->Surat_model->generateResi(),
					'no_surat' => $this->Surat_model->NoSurat('domisili')
				];
				$this->template->load('template', 'surat/form_domisili', $data);
			}else{
				$form = $this->input->post();
				$ceksurat = $this->Surat_model->ceksurat($form['jenis_surat'], $form['no_surat']);
				if ($ceksurat == 0) {
					$this->Surat_model->simpan($form);
				}
				$row = $this->T_penduduk_model->get_by_id($form['id_penduduk']);
		        if ($row) {
		        	$text = "KELURAHAN KARANGTENGAH\n \n NAMA : ".$row->nama."\n"."JENIS SURAT : ".strtoupper($form['jenis_surat'])."\n\nBetul Dikeluarkan oleh Kelurahan Karang Tengah dengan Nomor Resi ".$form['resi'];
		            $resi = chiper_encrypt('yulistiana', $text);
		            $resi2 = chiper_encrypt('yogi', $resi); 
		            $resi3 = chiper_encrypt ('syra', $resi2);
		            $resi =  base64_encode($resi3);
		            //var_dump($resi);
                    $params['data'] = $resi;
		        	$params['data'] = $resi;
					$params['level'] = 'H';
					$params['size'] = 2;
					$params['savename'] = FCPATH.'/assets/images/qrcode.png';
					$this->ciqrcode->generate($params);

		            $data = array(
						'id' => $row->id,
						'no_kk' => $row->no_kk,
						'no_nik' => $row->no_nik,
						'nama' => $row->nama,
						'tempat_lahir' => $row->tempat_lahir,
						'tgl_lahir' => $row->tgl_lahir,
						'jenis_k' => $row->jenis_k,
						'agama' => $row->agama,
						'pendidikan' => $row->pendidikan,
						'pekerjaan' => $row->pekerjaan,
						's_kawin' => $row->s_kawin,
						's_keluarga' => $row->s_keluarga,
						'kewarganegaraan' => $row->kewarganegaraan,
						'alamat' => $row->alamat,
						'rt' => $row->rt,
						'rw' => $row->rw,
						'kelurahan' => $row->kelurahan,
						'kecamatan' => $row->kecamatan,
						'kab_kota' => $row->kab_kota,
						'kode_pos' => $row->kode_pos,
						'provinsi' => $row->provinsi,
						'type_penduduk' => $row->type_penduduk,
						'last_update' => $row->last_update,
						'no_surat' => $form['no_surat'],
						'tgl_pengantar' => $form['tgl_pengantar'],
						'tujuan_pembuatan' => $form['tujuan_pembuatan'],
						'resi' => $form['resi'],
						'qrcode' => base_url().'assets/images/qrcode.png'
						);
		        }
				$this->template->load('previewSurat', 'surat/preview_domisili',$data);
			}
		}

		    //fungsi kematian
		public function peristiwa($preview = false){
			if ($preview == false) {
				$data = [
					'resi' => $this->Surat_model->generateResi(),
					'no_surat' => $this->Surat_model->NoSurat('kematian'),
					'penduduk' => $this->T_penduduk_model->get_all()
				];
				$this->template->load('template', 't_penduduk/form_kematian', $data);
			}else{
				$form = $this->input->post();
				$ceksurat = $this->Surat_model->ceksurat($form['jenis_surat'], $form['no_surat']);
				if ($ceksurat == 0) {
					$this->Surat_model->simpan($form);
				}
				$update_mati = [
					'type_penduduk' => 'meninggal'
				];
				$this->T_penduduk_model->update($form['id_penduduk'], $update_mati);
				$row = $this->T_penduduk_model->get_by_id($form['id_penduduk']);
		        if ($row) {
		        	$text = "KELURAHAN KARANGTENGAH\n \n NAMA : ".$row->nama."\n"."JENIS SURAT : ".strtoupper($form['jenis_surat'])."\n\nBetul Dikeluarkan oleh Kelurahan Karang Tengah dengan Nomor Resi ".$form['resi'];
		            $resi = chiper_encrypt('yulistiana', $text);
		            $resi2 = chiper_encrypt('yogi', $resi); 
		            $resi3 = chiper_encrypt ('syra', $resi2);
		            $resi =  base64_encode($resi3);
		            //var_dump($resi);
                    $params['data'] = $resi;
		        	$params['data'] = $resi;
					$params['level'] = 'H';
					$params['size'] = 2;
					$params['savename'] = FCPATH.'/assets/images/qrcode.png';
					$this->ciqrcode->generate($params);

		            $data = array(
						'id' => $row->id,
						'no_kk' => $row->no_kk,
						'no_nik' => $row->no_nik,
						'nama' => $row->nama,
						'tempat_lahir' => $row->tempat_lahir,
						'tgl_lahir' => $row->tgl_lahir,
						'jenis_k' => $row->jenis_k,
						'agama' => $row->agama,
						'pendidikan' => $row->pendidikan,
						'pekerjaan' => $row->pekerjaan,
						's_kawin' => $row->s_kawin,
						's_keluarga' => $row->s_keluarga,
						'kewarganegaraan' => $row->kewarganegaraan,
						'alamat' => $row->alamat,
						'rt' => $row->rt,
						'rw' => $row->rw,
						'kelurahan' => $row->kelurahan,
						'kecamatan' => $row->kecamatan,
						'kab_kota' => $row->kab_kota,
						'kode_pos' => $row->kode_pos,
						'provinsi' => $row->provinsi,
						'type_penduduk' => $row->type_penduduk,
						'last_update' => $row->last_update,
						'no_surat' => $form['no_surat'],
						'no_suratpengantar' => $form['no_suratpengantar'],
						'tgl_pengantar' => $form['tgl_pengantar'],
						'tujuan_pembuatan' => $form['tujuan_pembuatan'],
						'resi' => $form['resi'],
						'qrcode' => base_url().'assets/images/qrcode.png',
						'tanggal_kematian' => $form['tanggal_kematian'],
						'penyebab_kematian' => $form['penyebab_kematian']
						);
		        }
				$this->template->load('previewSurat', 'surat/preview_kematian',$data);
			}
		}

		public function form_sktm($preview = false){
			if ($preview == false) {
				$data = [
					'penduduk' => $this->T_penduduk_model->get_all(),
					'resi' => $this->Surat_model->generateResi(),
					'no_surat' => $this->Surat_model->NoSurat('sktm')
				];
				$this->template->load('template', 'surat/form_sktm', $data);
			}else{
				$form = $this->input->post();
				$ceksurat = $this->Surat_model->ceksurat($form['jenis_surat'], $form['no_surat']);
				if ($ceksurat == 0) {
					$this->Surat_model->simpan($form);
				}
				$row = $this->T_penduduk_model->get_by_id($form['id_penduduk']);
		        if ($row) {
		        	$text = "KELURAHAN KARANGTENGAH\n \n NAMA : ".$row->nama."\n"."JENIS SURAT : ".strtoupper($form['jenis_surat'])."\n\nBetul Dikeluarkan oleh Kelurahan Karang Tengah dengan Nomor Resi ".$form['resi'];
		            $resi = chiper_encrypt('yulistiana', $text);
		            $resi2 = chiper_encrypt('yogi', $resi); 
		            $resi3 = chiper_encrypt ('syra', $resi2);
		            $resi =  base64_encode($resi3);
		            //var_dump($resi);
                    $params['data'] = $resi;
		        	$params['data'] = $resi;
					$params['level'] = 'H';
					$params['size'] = 2;
					$params['savename'] = FCPATH.'/assets/images/qrcode.png';
					$this->ciqrcode->generate($params);

		            $data = array(
						'id' => $row->id,
						'no_kk' => $row->no_kk,
						'no_nik' => $row->no_nik,
						'nama' => $row->nama,
						'tempat_lahir' => $row->tempat_lahir,
						'tgl_lahir' => $row->tgl_lahir,
						'jenis_k' => $row->jenis_k,
						'agama' => $row->agama,
						'pendidikan' => $row->pendidikan,
						'pekerjaan' => $row->pekerjaan,
						's_kawin' => $row->s_kawin,
						's_keluarga' => $row->s_keluarga,
						'kewarganegaraan' => $row->kewarganegaraan,
						'alamat' => $row->alamat,
						'rt' => $row->rt,
						'rw' => $row->rw,
						'kelurahan' => $row->kelurahan,
						'kecamatan' => $row->kecamatan,
						'kab_kota' => $row->kab_kota,
						'kode_pos' => $row->kode_pos,
						'provinsi' => $row->provinsi,
						'type_penduduk' => $row->type_penduduk,
						'last_update' => $row->last_update,
						'no_surat' => $form['no_surat'],
						'tgl_pengantar' => $form['tgl_pengantar'],
						'tujuan_pembuatan' => $form['tujuan_pembuatan'],
						'resi' => $form['resi'],
						'qrcode' => base_url().'assets/images/qrcode.png'
						);
		        }
				$this->template->load('previewSurat', 'surat/preview_sktm',$data);
			}
		}
	}
	?>