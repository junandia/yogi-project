<div class="content-wrapper">
	<section class="content">
		<div class="box box-warning box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">FORM PENDUDUK</h3>
			</div>
			<form action="<?php echo $action; ?>" method="post">
				<table class='table table-bordered'>

					<tr>
						<td width='200'>
							No Kk <?php echo form_error('no_kk') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="no_kk" id="no_kk" placeholder="No Kk" value="<?php echo $no_kk; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							No Nik <?php echo form_error('no_nik') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="no_nik" id="no_nik" placeholder="No Nik" value="<?php echo $no_nik; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Nama <?php echo form_error('nama') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Tempat Lahir <?php echo form_error('tempat_lahir') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" value="<?php echo $tempat_lahir; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Tgl Lahir <?php echo form_error('tgl_lahir') ?>
								
							</td>
						<td>
							<input type="date" class="form-control" name="tgl_lahir" id="tgl_lahir" placeholder="Tgl Lahir" value="<?php echo $tgl_lahir; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Jenis Kelamin <?php echo form_error('jenis_k') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="jenis_k" id="jenis_k" placeholder="Jenis K" value="<?php echo $jenis_k; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Agama <?php echo form_error('agama') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="agama" id="agama" placeholder="Agama" value="<?php echo $agama; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Pendidikan <?php echo form_error('pendidikan') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="pendidikan" id="pendidikan" placeholder="Pendidikan" value="<?php echo $pendidikan; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Pekerjaan <?php echo form_error('pekerjaan') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="pekerjaan" id="pekerjaan" placeholder="Pekerjaan" value="<?php echo $pekerjaan; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							S Kawin <?php echo form_error('s_kawin') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="s_kawin" id="s_kawin" placeholder="S Kawin" value="<?php echo $s_kawin; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							S Keluarga <?php echo form_error('s_keluarga') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="s_keluarga" id="s_keluarga" placeholder="S Keluarga" value="<?php echo $s_keluarga; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Kewarganegaraan <?php echo form_error('kewarganegaraan') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="kewarganegaraan" id="kewarganegaraan" placeholder="Kewarganegaraan" value="<?php echo $kewarganegaraan; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Alamat <?php echo form_error('alamat') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="<?php echo $alamat; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Rt <?php echo form_error('rt') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="rt" id="rt" placeholder="Rt" value="<?php echo $rt; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Rw <?php echo form_error('rw') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="rw" id="rw" placeholder="Rw" value="<?php echo $rw; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Kelurahan <?php echo form_error('kelurahan') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="kelurahan" id="kelurahan" placeholder="Kelurahan" value="<?php echo $kelurahan; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Kecamatan <?php echo form_error('kecamatan') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="kecamatan" id="kecamatan" placeholder="Kecamatan" value="<?php echo $kecamatan; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Kab Kota <?php echo form_error('kab_kota') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="kab_kota" id="kab_kota" placeholder="Kab Kota" value="<?php echo $kab_kota; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Kode Pos <?php echo form_error('kode_pos') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="kode_pos" id="kode_pos" placeholder="Kode Pos" value="<?php echo $kode_pos; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							Provinsi <?php echo form_error('provinsi') ?>
								
							</td>
						<td>
							<input type="text" class="form-control" name="provinsi" id="provinsi" placeholder="Provinsi" value="<?php echo $provinsi; ?>" />
							
						</td>
					</tr>
					<tr>
						<td width='200'>
							<?php
								echo ($this->uri->segment('2') == 'update') ? 'Type Penduduk' : '' ; 
								echo form_error('type_penduduk') 
							?>
								
							</td>
						<td>
							<?php
								if ($this->uri->segment('2') == 'update') {
							?>
							<select name="type_penduduk" class="form-control">
								<option value="hidup">HIDUP</option>
								<option value="meninggal">MENINGGAL</option>
							</select>
							<?php
								}else{
									echo '<input type="hidden" name="type_penduduk" value="hidup">';
								}
							?>
						</td>
					</tr>
					<tr>
						<td width='200'>
							<?php echo form_error('last_update') ?>
								
							</td>
						<td>
							<input type="hidden" class="form-control" name="last_update" id="last_update" placeholder="Last Update" value="<?php echo date('Y-m-d H:i:s'); ?>" />
							
						</td>
					</tr>
					<tr>
						
					<td>

						
					</td>
					<td>
						<input type="hidden" name="id" value="<?php echo $id; ?>" /> 
							<button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i> <?php echo $button ?></button> 
							<a href="<?php echo site_url('t_penduduk') ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> Kembali</a>
								
							</td>
					</tr>
				</table>
			</form>        
		</div>
	</section>
</div>