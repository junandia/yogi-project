<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning box-solid">
    
                    <div class="box-header">
                        <h3 class="box-title">KELOLA DATA PENDUDUK</h3>
                    </div>
        
        <div class="box-body">
            <div class='row'>
            <div class='col-md-9'>
            <div style="padding-bottom: 10px;">
        <?php echo anchor(site_url('t_penduduk/create'), '<i class="fa fa-wpforms" aria-hidden="true"></i> Tambah Data', 'class="btn btn-danger btn-sm"'); ?>
		<?php echo anchor(site_url('t_penduduk/excel'), '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Ms Excel', 'class="btn btn-success btn-sm"'); ?></div>
            </div>
            <div class='col-md-3'>
            <form action="<?php echo site_url('t_penduduk/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('t_penduduk'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
            </div>
        
   
        <div class="row" style="margin-bottom: 10px">
            <div class="text-center">
                <div style="margin-top: 8px" id="message">
                    <?php 
                        if ($this->session->userdata('message') != '') {
                            echo alert('alert-success', '', $this->session->userdata('message') <> '' ? $this->session->userdata('message') : 'List Data Penduduk'); 
                        }
                    ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>No Kk</th>
		<th>No Nik</th>
		<th>Nama</th>
		<th>Tempat Lahir</th>
		<th>Tgl Lahir</th>
		<th>Jenis K</th>
		<th>Rt</th>
		<th>Rw</th>
		<th>Type Penduduk</th>
		<th>Last Update</th>
		<th>Action</th>
            </tr><?php
            foreach ($t_penduduk_data as $t_penduduk)
            {
                ?>
                <tr>
			<td width="10px"><?php echo ++$start ?></td>
			<td><?php echo $t_penduduk->no_kk ?></td>
			<td><?php echo $t_penduduk->no_nik ?></td>
			<td><?php echo $t_penduduk->nama ?></td>
			<td><?php echo $t_penduduk->tempat_lahir ?></td>
			<td><?php echo $t_penduduk->tgl_lahir ?></td>
			<td><?php echo $t_penduduk->jenis_k ?></td>
			<td><?php echo $t_penduduk->rt ?></td>
			<td><?php echo $t_penduduk->rw ?></td>
			<td><?php echo $t_penduduk->type_penduduk ?></td>
			<td><?php echo $t_penduduk->last_update ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('t_penduduk/update/'.$t_penduduk->id),'<i class="fa fa-pencil-square-o" aria-hidden="true"></i>','class="btn btn-danger btn-sm"'); 
				echo '  '; 
				echo anchor(site_url('t_penduduk/delete/'.$t_penduduk->id),'<i class="fa fa-trash-o" aria-hidden="true"></i>','class="btn btn-danger btn-sm" Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
        </div>
                    </div>
            </div>
            </div>
    </section>
</div>