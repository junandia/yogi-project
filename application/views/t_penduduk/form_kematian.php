<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-warning box-solid">

					<div class="box-header">
						<h3 class="box-title">Form Kematian</h3><hr/>
					</div>

					<div class="box-body">
						<?php echo form_open('Surat/peristiwa/preview') ?>
						<input type="hidden" name="jenis_surat" value="kematian">
						<input type="hidden" name="no_surat" value="<?= $no_surat ?>">
						<input type="hidden" name="resi" value="<?= $resi ?>">
						<input type="hidden" name="created_at" value="<?= date('Y-m-d H:i:s') ?>">
							<table class="table">
								<tr>
									<td width="200">Nama Warga</td>
									<td>
										<select name="id_penduduk" class="js-example-basic-single js-states form-control" id="nik" placeholder="TEST">
											<?php
											echo "<option value ='' readonly>Pilih</option>";
											foreach ($penduduk as $data_penduduk) {
												echo "<option value='".$data_penduduk->id."'>".$data_penduduk->nama." - ".$data_penduduk->no_nik."</option>";
											}
											?>
										</select>
										<a href="<?= base_url('index.php/T_penduduk/create') ?>" target="_blank">Tambah Warga</a>
									</td>
								</tr>
								<tr>
									<td width="200">Tanggal - Jam Kematian</td>
									<td><input type="date" name="tanggal_kematian"> - <input type="time" name="jam_kematian"></td>
								</tr>
								<tr>
									<td width="200">Penyebab Kematian</td>
									<td><input type="text" name="penyebab_kematian" class="form-control" ></td>
								</tr>
								<tr>
									<td width="200"><label>Nomor Surat Pengantar</label></td>
									<td><input type="text" name="no_suratpengantar" class="form-control" placeholder="Masukan Nomor Surat Pengantar" required=""></td>
								</tr>
								<tr>
									<td width="200"><label>Tanggal Surat Pengantar</label></td>
									<td><input type="date" name="tgl_pengantar" class="form-control" value="<?= date('Y-m-d') ?>" required=""></td>
								</tr>
								<tr>
									<td><label>Tujuan Pembuatan</label></td>
									<td><input type="text" name="tujuan_pembuatan" class="form-control"></td>
								</tr>
								<tr>
									<td colspan="2">
										<input type="submit" name="submit_form" value="Simpan dan Cetak" class="btn btn-primary">
									</td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>