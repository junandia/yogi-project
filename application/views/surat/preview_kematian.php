<!DOCTYPE html>
<html>
<head>
	<title>SURAT KETERANGAN LAPORAN KEMATIAN</title>
</head>
<body>
	<table id="kop" align="center" width="700px">
		<tr style="text-align: center;">
			<td colspan="2"><img src="<?php echo base_url('assets/images/KOP.png') ?>"></td>
		</tr>
		<tr style="text-align: center;">
			<td colspan="2"><center><b><u>SURAT KETERANGAN LAPORAN KEMATIAN</u></b></center></td>
		</tr>
		<tr style="text-align: center;">
			<td colspan="2"><center>NOMOR : 472.3/<?php echo $no_surat ?>/1.1002/<?php echo date('Y') ?></center></td>
		</tr>
		<tr>
			<td colspan="2"><br/></td>
		</tr>
		<tr>
			<td colspan="2">Lurah Karangtengah Kecamatan Gunungpuyuh Kota Sukabumi,  dengan ini menerangkan bahwa:
			</td>
		</tr>
		<tr>
			<td><br/></td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>: <?= $nama ?></td>
		</tr>
		<tr>
			<td>No. NIK/KTP</td>
			<td>: <?= $no_nik ?></td>
		</tr>
		<tr>
			<td>Tempat, Tanggal Lahir</td>
			<td>: <?= $tempat_lahir.", ".date('d-m-Y', strtotime($tgl_lahir)) ?></td>
		</tr>
		<tr>
			<td>Jenis Kelamin</td>
			<td>: <?= $jenis_k ?></td>
		</tr>
		<tr>
			<td>Kewarganegaraan</td>
			<td>: <?= $kewarganegaraan ?></td>
		</tr>
		<tr>
			<td>Pekerjaan</td>
			<td>: <?= $pekerjaan ?></td>
		</tr>
		<tr>
			<td>Status Perkawinan</td>
			<td>: <?= $s_kawin ?></td>
		</tr>
		<tr>
			<td>Agama</td>
			<td>: <?= $agama ?></td>
		</tr>
		<tr>
			<td>Alamat</td>
			<td>: <?= $alamat." RT".$rt."/".$rw." Kel. ".$kelurahan." Kec.".$kecamatan." ".$kab_kota ?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><br/><center>Adalah benar Warga <?= "RT".$rt."/".$rw." Kel. ".$kelurahan." Kec.".$kecamatan." ".$kab_kota ?>.</center><br/></td>
	</tr>
	<tr>
		<td colspan="2">Diterangkan lebih lanjut berdasarkan Surat Pengantar Laporan Kematian dari Ketua RT/RW setempat Nomor : <?php echo $no_suratpengantar ?> Tanggal <?php echo $tgl_pengantar ?> dan Surat Pernyataan Ahli Waris di atas Materai, adalah benar bahwa nama tersebut di atas sudah meninggal dunia pada Tanggal <?php echo $tanggal_kematian ?> bertempat di Sukabumi dikarenakan <?php $penyebab_kematian ?>, dan telah di makamkan di Sukabumi pada hari yang sama.</td>
	</tr>
	<tr style="text-align: center;">
		<td colspan="2"><center>Surat keterangan ini dipergunakan untuk : <br/>
			<b><?= $tujuan_pembuatan ?></center></b>
		</td>
	</tr>
	<tr>
		<td><br/></td>
	</tr>
	<tr>
		<td colspan="2">Demikian Surat Keterangan ini dibuat untuk dipergunakan sebagaimana mestinya dan berlaku sampai dengan : <?= date('d-m-Y', strtotime('+1 Month')) ?>.</td>
	</tr>
	<tr>
		<td><br/></td>
	</tr>
	<tr>
		<td><br/></td>
	</tr>
	<tr >
		<td style="text-align: left;">
			
		</td>
		<td style="text-align: right;">
			Ditetapkan di 	:	Sukabumi<br/>
			Pada Tanggal 	:	<?= date('d-m-Y') ?><br/>
			Lurah Karangtengah
			<br/>
			<br/>
			<img src="<?php echo $qrcode; ?>">
			<br/>
			<br/>
			TONI SLAMET, S.IP<br/>
			NIP. 19620606 199003 1015

		</td>
	</tr>
	<tr>
		<td><br/></td>
	</tr>
	<tr>
		<td colspan="2"><center><em>Dokumen ini hasil cetakan komputer dan sah serta tidak memerlukan tandatangan basah.</em></center></td>
	</tr>
</table>
<script type="text/javascript">
	window.print()
</script>