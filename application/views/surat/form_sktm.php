<div class="content-wrapper">
	<section class="content">
		<div class="box box-warning box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">FORM SURAT KETERANGAN TIDAK MAMPU</h3>
			</div>
			<form method="POST" action="<?php echo base_url('index.php/Surat/form_sktm/preview') ?>">
				<input type="hidden" name="jenis_surat" value="sktm">
				<input type="hidden" name="no_surat" value="<?= $no_surat ?>">
				<input type="hidden" name="resi" value="<?= $resi ?>">
				<input type="hidden" name="created_at" value="<?= date('Y-m-d H:i:s') ?>">
				<table class="table">
					<tr>
						<td width="200"><label>Nomor Surat Pengantar</label></td>
						<td><input type="text" name="no_suratpengantar" class="form-control" placeholder="Masukan Nomor Surat Pengantar" required=""></td>
					</tr>
					<tr>
						<td width="200"><label>Tanggal Surat Pengantar</label></td>
						<td><input type="date" name="tgl_pengantar" class="form-control" value="<?= date('Y-m-d') ?>" required=""></td>
					</tr>
					<tr>
						<td width="200"><label>Nama Warga</label></td>
						<td>
							<select name="id_penduduk" class="js-example-basic-single js-states form-control" id="nik" placeholder="TEST">
								<?php
								echo "<option value ='' readonly>Pilih</option>";
								foreach ($penduduk as $data_penduduk) {
									echo "<option value='".$data_penduduk->id."'>".$data_penduduk->nama." - ".$data_penduduk->no_nik."</option>";
								}
								?>
							</select>
							<a href="<?= base_url('index.php/T_penduduk/create') ?>" target="_blank">Tambah Warga</a>
						</td>
					</tr>
					<tr>
						<td><label>Tujuan Pembuatan</label></td>
						<td><input type="text" name="tujuan_pembuatan" class="form-control"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" name="submit_form" class="btn btn-primary" value="Simpan dan Cetak"></td>
					</tr>
				</table>
			</form>
		</div>
	</section>
</div>