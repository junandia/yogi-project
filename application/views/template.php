<!DOCTYPE HTML>
<html>
<head>
    <title>Kelurahan Karangtengah</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Baxster Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
    SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url('assets/') ?>css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="<?= base_url('assets/') ?>css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <link rel="icon" href="favicon.ico" type="image/x-icon" >
    <!-- font-awesome icons -->
    <link href="<?= base_url('assets/') ?>css/font-awesome.css" rel="stylesheet"> 
    <!-- //font-awesome icons -->
    <!-- chart -->
    <script src="<?= base_url('assets/') ?>js/Chart.js"></script>
    <!-- //chart -->
    <!-- js-->
    <script src="<?= base_url('assets/') ?>js/jquery-1.11.1.min.js"></script>
    <script src="<?= base_url('assets/') ?>js/modernizr.custom.js"></script>
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts--> 
    <!--animate-->
    <link href="<?= base_url('assets/') ?>css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="<?= base_url('assets/') ?>js/wow.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css ">
    <script>
       new WOW().init();
   </script>
   <!--//end-animate-->
   <!-- Metis Menu -->
   <script src="<?= base_url('assets/') ?>js/metisMenu.min.js"></script>
   <script src="<?= base_url('assets/') ?>js/custom.js"></script>
   <link href="<?= base_url('assets/') ?>css/custom.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/adminlte/bower_components/select2/dist/css/select2.min.css">
   <!--//Metis Menu -->
</head> 
<body class="cbp-spmenu-push">
    <div class="main-content">
        <!--left-fixed -navigation-->
        <div class="sidebar" role="navigation">
            <div class="navbar-collapse">
                <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right dev-page-sidebar mCustomScrollbar _mCS_1 mCS-autoHide mCS_no_scrollbar" id="cbp-spmenu-s1">
                    <div class="scrollbar scrollbar1">
                        <ul class="nav" id="side-menu">
                            <li>
                                <a href="<?php echo base_url('index.php/Welcome') ?>" class="active"><i class="fa fa-home nav_icon"></i>Beranda</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-check-square-o nav_icon"></i>Master Data<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li>
                                        <a href="<?php echo base_url('index.php/T_penduduk') ?>">Data Warga</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('index.php/User') ?>">Data Pengguna</a>
                                    </li>
                                </ul>
                                <!-- //nav-second-level -->
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-envelope nav_icon"></i>Buat Surat<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li>
                                        <a href="<?php echo base_url('index.php/Surat/form_domisili') ?>">Domisili</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('index.php/Surat/form_sktm') ?>">Keterangan Tidak Mampu</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('index.php/Surat/peristiwa') ?>">Peristiwa Kematian</a>
                                    </li>
                                </ul>
                                <!-- //nav-second-level -->
                            </li>
                            <li>
                                <a href="<?php echo base_url('index.php/Arsip') ?>" class="chart-nav"><i class="fa fa-list-ul nav_icon"></i>Arsip Surat</a>
                                <!-- //nav-second-level -->
                            </li>
                        </ul>
                    </div>
                    <!-- //sidebar-collapse -->
                </nav>
            </div>
        </div>
        <!--left-fixed -navigation-->
        <!-- header-starts -->
        <div class="sticky-header header-section ">
            <div class="header-left">
                <!--logo -->
                <div class="logo">
                    <a href="#">
                        <ul>    
                            <li><img src="images/logo1.png" alt="" /></li>
                            <li><h1>KELURAHAN KARANG TENGAH</h1></li>
                            <div class="clearfix"> </div>
                        </ul>
                    </a>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"> </div>
                <!--//logo-->
                <div class="header-right header-right-grid">
                    <div class="profile_details_left"><!--notifications of menu start -->

                        <div class="clearfix"> </div>
                    </div>
                </div>
                
                
                <div class="clearfix"> </div>
            </div>
            <!--search-box-->
            <!--//end-search-box-->
            <div class="clearfix"> </div>   
        </div>
        <!-- //header-ends -->
        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
                <!-- four-grids -->
                <div class="row four-grids">
                    <?php
                    echo $contents;
                    ?>
                </div>
            </div>                  
        </div>
        <!--footer-->
        <div class="dev-page">

            <!-- page footer -->   
            <!-- dev-page-footer-closed dev-page-footer-fixed -->
            <div class="dev-page-footer dev-page-footer-fixed"> 
                <!-- container -->
                <div class="container">
                    <div class="copyright">
                        <p>© 2020 M Yogi P</p> 
                    </div>
                    <!-- //page footer buttons -->
                    <!-- page footer container -->
                </div>
                <!-- //container -->
            </div>
            <!-- /page footer -->
        </div>
        <!--//footer-->
    </div>
    <!-- Classie -->
    <script src="<?= base_url('assets/') ?>js/classie.js"></script>
    <script>
        var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        showLeftPush = document.getElementById( 'showLeftPush' ),
        body = document.body;

        showLeftPush.onclick = function() {
            classie.toggle( this, 'active' );
            classie.toggle( body, 'cbp-spmenu-push-toright' );
            classie.toggle( menuLeft, 'cbp-spmenu-open' );
            disableOther( 'showLeftPush' );
        };


        function disableOther( button ) {
            if( button !== 'showLeftPush' ) {
                classie.toggle( showLeftPush, 'disabled' );
            }
        }
    </script>
    <!-- Bootstrap Core JavaScript --> 

    <script type="text/javascript" src="<?= base_url('assets/') ?>js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?= base_url('assets/') ?>js/dev-loaders.js"></script>
    <script type="text/javascript" src="<?= base_url('assets/') ?>js/dev-layout-default.js"></script>
    <link href="<?= base_url('assets/') ?>css/bootstrap.min.css" rel="stylesheet">

    <!-- candlestick -->
    <script type="text/javascript" src="<?= base_url('assets/') ?>js/jquery.jqcandlestick.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/') ?>css/jqcandlestick.css" />
    <!-- //candlestick -->

    <!--max-plugin-->
    <script type="text/javascript" src="<?= base_url('assets/') ?>js/plugins.js"></script>
    <!--//max-plugin-->

    <!--scrolling js-->
    <script src="<?= base_url('assets/') ?>js/jquery.nicescroll.js"></script>
    <script src="<?= base_url('assets/') ?>js/scripts.js"></script>
    <!--//scrolling js-->

    <!-- real-time-updates -->
    <script language="javascript" type="text/javascript" src="<?= base_url('assets/') ?>js/jquery.flot.js"></script>

    <!-- error-graph -->
    <script language="javascript" type="text/javascript" src="<?= base_url('assets/') ?>js/jquery.flot.errorbars.js"></script>
    <script language="javascript" type="text/javascript" src="<?= base_url('assets/') ?>js/jquery.flot.navigate.js"></script>
    <!-- Skills-graph -->       
    <script src="<?= base_url('assets/') ?>js/Chart.Core.js"></script>
    <script src="<?= base_url('assets/') ?>js/Chart.Doughnut.js"></script>
    <!-- status -->
    <script src="<?= base_url('assets/') ?>js/jquery.fn.gantt.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jquery-ui/ui/minified/jquery-ui.min.js"></script>
        <!-- jQuery 3
        <script src="<?php echo base_url() ?>assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    -->
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url() ?>assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() ?>assets/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() ?>assets/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() ?>assets/adminlte/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() ?>assets/adminlte/dist/js/demo.js"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url() ?>assets/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
    <!-- page script -->
    <script>
        $(function () {
            $('.select2').select2()
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })

        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });
    </script>

</body>
</html>